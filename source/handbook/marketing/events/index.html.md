---
layout: markdown_page
title: "GitLab Event Information"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# Events at GitLab 

There are 3 groups within marketing who handle external events. Each group has a specific purpose. Please review each page for specific details.

* [Community Relations](/handbook/marketing/community-relations/evangelist-program/) 
* [Corporate Events](/handbook/marketing/corporate-marketing/#corporate-events)
* [Field Marketing](/handbook/marketing/revenue-marketing/field-marketing/) 

## Which events is GitLab already sponsoring? 
* Internal tracking of events, please add our [Events and Sponsorship Calendar](https://calendar.google.com/calendar?cid=Z2l0bGFiLmNvbV9laWN2b3VkcHBjdTQ3bG5xdTFwOTlvNjU2Z0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t) to your calendar. 
* External facing event tracker can be found at https://about.gitlab.com/events/ 

## Suggesting an Event 
  
To determine who would handle the event, please refer to our [events decision tree](https://docs.google.com/spreadsheets/d/1aWsmsksPfOlX1t6TeqPkh5EQXergt7qjHAjGTxU27as/edit#gid=0). If it is not clear who should own an event based on the decision tree, please email `events@gitlab.com`. 

Please *only request* event support/sponsorship if your proposed event fits the following criteria:
The event will further business aims of GitLab. 
The event has an audience of **250+ people** (the exception being meet-ups (which are run by our community team) or is part of an Account Based Marketing activity.
The event is a more than a month away.


* If your event fits the criteria above and you would like support from marketing, create an issue in the appropriate marketing project. 
* [Community Relations](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/issues/new?issuable_template=sponsorship-request) 
* [Corporate Events](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues/new?issuable_template=Corporate-Event-Request)
* [Field Marketing](https://gitlab.com/gitlab-com/marketing/field-marketing/issues/new?issuable_template=Event_Field_Marketing)

Be sure you review the issue template and provide all necessary information that is asked of you in the issue.

## How We Evaluate and Build Potential Events
All GitLab events must check at least drive two or more of the aims of our events below to be considered.
 - Brand awareness- we want to be a household name by 2020!
 - Build community
 - Gain contributors
 - Thought leadership
 - Help with hiring
 - Gather new relevant leads/ drive ROI 
 - Educate possible buyers or users on our product or features
 - Marketplace positioning
 - Partnerships/ Alliances

## Questions we ask ourselves when assessing an event:
- How and where will this position us as a brand?
- Does this event drive business goals forward in the next quarter? Year?
- Is the event important for the industry, thought leadership, or brand visibility? We give preference to events that influence trends and attract leaders and decision makers. We also prioritize events organized by our strategic partners.
- Will there be a GitLab speaker? We do not require a speaker slot in return for sponsorship but we do prioritize events where the audience will be hearing about GitLab - either from a GitLab team-member or a member of the wider GitLab community.
- What type of people will be attending the event? We prefer events attended by  diverse groups of decision makers with an interest in DevOps, DevSecOps, Cloud Native, Kubernetes, Serverless, Multi-cloud, CI/CD, Open Source, and other related topics.
- Will we be able to interact with attendees? We stress events that provide opportunities for meetings, workshops, booth or stands to help people find us, and other interaction with attendees.
- Where will the event be held? We aim to have a presence at events around the globe with a particular focus on areas with large GitLab communities and large populations of support.
- What is the size of the opportunity for the event? We prioritize events based their potential reach (audience size, the number of interactions we have with attendees).
- What story do we have to tell here and how does the event fit into our overall company strategy, goals, and product direction?
- Do we have the bandwidth and resources to make this activity a success? Do we have the cycles, funds, collateral and runway to invest fully and make this as successful as possible? Event must be weighed against other current activity in region and department.

Suggested events will be subject to a valuation calculation- will it meet or exceed objectives listed above?

### For Corporate Marketing - Event Scorecard
Each question above is graded on a scale of 0-2. We then tally the scores and assign the event to a sponsorship tier.

- Events scoring below 8 are not eligible for corporate sponsorship or financial support.
- Events scoring 10+ are given top priority for staffing, and resources.

| Criteria / Score   | 0 | 1  | 2 |
|----------------|---------------|---------------|----------------|
| Thought Leadership |  |  |  |
| Audience type |  |  |  |
| Attendee interaction |  |  |  |
| Location and Timing |  |  |  |
| Event Relevance/ Strategy |  |  |  |
| Brand Reach |  |  |  |
| Opportunity size/ Potential ROI |  |  |  |

We ask these questions and use this scorecard to ensure that we're prioritizing the GitLab's brand and our community's best interests when we sponsor events.

## Event Execution 
After its been decided by the DRI (using the above criteria) that we will move forward with the sponsoring of an event, what happens next? 
1.  DRI for event will start finance issue for contract review and signature following the [Procure to pay instructions](/handbook/finance/procure-to-pay/) 
* Be sure to include:
    * ROI calculation for said event in the final cost section
    * Add the finance issue as a `Related issue` to the original issue for reference. 
2. As soon as the contract has been signed, DRI needs to change the status of the issue tag from "status: plan" to "status: WIP". This let's the designated event MPM know to begin their backend execution. He/She will add a check list of issues to be created and info they need to create said issues and process. They will also create the Epic and associate everything existing to epic.
3. For corporate events, the DRI needs to create the Event planning issue, using "Corporate Event Planner Issue" template for tracking progress towards execution.
4. Add the event to Events Cal and Events Page. Instructions can be found here. 
5. Start checking off the appropriate issue template. Some things to note as you go through process in template:
    * Once you select staffing, start a slack channel and invite those folks as well as anyone from Field Marketing (FM) or alliance that needs to be involved. Do not link anything but the epic in the slack channel as it will be deleted after 90 days.
    * Share the planning sheet in the slack channel for people to fill out their contact and travel info. Instruct everyone to book travel and lodging ASAP and add to planning sheet.
    * The planning sheet is used for everything from Travel, meeting setting, booth duty, speakers list, networking events, PR...
6. Once the Epic created... (the Epic is the main hub for all event info. ALL ISSUES ASSOCIATED WITH THE EVENT MUST BE LINKED TO THE EPIC!)
    * Link to Original Issue (some may call this a META issue)
    * [Add the planning sheet](https://docs.google.com/spreadsheets/d/1i2-CdlsvW2x98NvJJ1mLcVq6ymehaRqSu2ckkWiV5ko/edit#gid=812678489&range=A1)
    * Link to event landing page
    * Add high level booth hours
    * Booth Number (should be in epic name)
    * Any other high level info that will be relevant to everyone attending.
7. If the event needs a speaker, start an issue with the speaker request issue template and tag tech evangelism.
8. Landing Pages for Events
    * Corporate events uses landing pages built from the GitLab events page. The MPM will create an issue for content to be provided on that page (work with Alliances/ PMM on copy) and it will need to be decided in collaboration with FM if we want a form on the landing page. (more info on this can be found in the MPM handbook)
    * For Field Marketing events we use Marketo landing pages vs. the events yml. By doing this, the MPMs own the creation of these pages and they are the only ones who will have edit access to these pages.

When to specifically use a Marketo landing page vs. the events yml:
1. This is an event owned by Field Marketing
1. The event cost the company less than $10,000 (or your country's equivalent)
1. We will be driving traffic to the marketo landing page for less than 1.5 months.

9. Schedule
    1. Event kick off call (include all people involved in planning), around 2 months out from event.
    2. Final event check in meeting (include everyone attending, anyone from Alliances involved, PMM who created demos to review them with team)
    3. Event Recap (with all planners/ stakeholders)
10. Copy needed
    1. Landing page copy
    2. 3-4 weeks out email invite copy
    2. 1-2 weeks out post event copy
11. Social
    * Start issue using the social request template for general social awareness posts and any social ads that need to go out.
    * For complete instructions on how to obtain social support at your event, please review the [social requests instructions](/handbook/marketing/corporate-marketing/social-marketing/#requesting-social-promotion-). 
12. Design
    1. Issue needed for booth design (in corporate marketing project). Tag Designer and provide design specs as well as due date. Give as much notice as possible.
    2. For the latest approved booth design/ messaging ping events@gitlab.com. For any content or major layout changes start an issue in corporate marketing and tag PMM and Design.
13. Digital
    * Coordinate all digital marketing requests with your MPM. See [Requesting Digital Marketing Promotions](/handbook/marketing/revenue-marketing/digital-marketing-programs/digital-marketing-management/#requesting-digital-marketing-promotions) for more info.
14. For Corporate events - Meeting Setting
    * Most corporate events will have an onsite meeting setting initiative tied to the event goals.
    * The FMM is DRI for the ancillary field activities, like meeting setting and customer dinners.
    * Corporate marketing will work with the regional FM DRI and the designated event MPM to decide on the best plan of attack for meeting setting.
    * If any execs are on site, all meetings with them should be coordinated through designated EA lead for that event.
    * We track meetings on the master event spreadsheet. This sheet will be locked 24 hours before event starts- people will only be able to make comments. If you need to request a change on this doc take the FM lead.
    * All one site meetings must have a [meeting prep doc](https://docs.google.com/document/d/1QZB8m99Lt5GVnBDcfST1otmA81uH6EMEMkF-1zhQ0q0/edit), which can be found in the planning sheet. Note, we share these prep docs with the client. This sheet is intended to provide everyone attending the meeting with any background info on the prospect/ customer they should know going into the meeting, as well as any objectives or things to cover in said meeting. This sheet can be found in Google Docs as well as in every event issue.
    * All leads gathered through meeting setting initiatives must be tracked in their own campaign.
    * We generally like to provide a small gift (unedr $50) for anyone who takes a meeting with us.
15. Demos, booth decks, and documentation
    * Product marketing helps to make all displayed collateral at events.
    * These are the standard [demos](/handbook/marketing/product-marketing/demo/) we use/ present at events. They should be preloaded on event iPads.
    * If you need something specific for a show, start an issue in the PMM project and tag Dan Gordon. They need at least 3 weeks to produce something custom.
16. Swag- decide appropriate giveaway for this event and audience. Coordinate ordering with one of our preferred swag vendors.
    * Order extra storage at event if all swag will not fit in booth
17. Leads and Campaign setup
    * DRI is  responsible for pulling, cleaning and sharing ead list with MPM and ops within 24 hours of event close. Use temples for clean up provided by MPM.

VERY IMPORTANT: Note this planning list is not exhaustive- see planning issue template in both & field marketing project for most up to date list of tasks.

### How We Decide Who Attends Which Events?

* The event DRI determines how many staffers we need at the event and is responsible for ensuring the staffers are all set to attend the event.
* If the event is more enterprise-focused we try to send more marketing/ sales. Regional Sales Managers in partnership with FM select staffer based on who has the most potential contacts in the area or going to an event.
* If the event is more user-focused we will lean towards sending more technical people to staff and fewer sales.
* Suggestion for staffing: See if we have any GitLabbers who live in the area who might be a good fit for the audience.
* We lean towards those who might be thought leaders, specialists, or more social in the specific show - i.e. if we are sponsoring an AWS show, we would like for a GitLab + AWS expert to staff the event.
* We aim to bring minimal staff to keep costs and disruption to normal workflow low. We take into account what value everyone will provide as well as staffing balance. Please check with the event DRI if you would like to or would like to suggest someone participate in an event.
* Event staffing list will close 2 weeks before commencement of event.
* Once you have agreed to attend an event, you are not able to back out unless there is a customer facing obligation you need to tend to. We have this in place to avoid unnecessary rework on the event DRI’s behalf.
* A lot of times a technical resource needs to also be assigned to attend an event. In order to do so, please review the SA handbook for [instructions](/handbook/customer-success/solutions-architects/#when-and-how-to-engage-a-solutions-architect) on how to secure one of our awesome SA's.
* All those attending will need their manager's approval.
* If you have been approved by the DRI and your manager to help staff an event, all your travel will be included during the time for the event/ expo days. You need to be onsite and ready to help out as soon as the first expo hall shift opens up and you may book travel any time after the expo hall closes. We will cover the night of lodging before the expo hall opens through to the night it closes. Any additional nights will need to be covered by the individual.
* Event staffing list will close 2 weeks before commencement of the event (3 weeks for corporate events.
* If you are not officially involved in the event as part of the sponsorship, we would still like to know you will be attending so we can include you in any activities we have going on. Please comment in the respective event slack channel letting us know your plans to attend after obtaining approval from your manager or comment in the epic for the event.

### Event Outreach
It is important that we are communicating with our customers and prospects when are attending an event. This is done through a targeted email sent through Marketo & also through SDR & SAL outreach. 

- Receive attendee list and contact customers and prospects before event using talking points provided by content DRI with the goal of setting up meetings/ demos at the event. Invite them to anything specific we have happening at or around event.
- If there is not an attendee list process is as follows:
 - Target speakers for outreach.
 - Utilize previous years attendee list that can be found in SFDC. That person may not be attending, but their colleague might be. Ask for intros.
 - Follow event hashtags to see who will be attending.
 - Join local meetup and pre event events (this works well for large events like AWS).
 - Join LinkedIn Groups and slack channels dedicated to event.
 - Download event app and engage with attendees via app.
    - You can search for relevant talks and see who has registered for talks that might apply to potential customers.

#### For Field Events
-  Adding Records to the Campaign in order for the record to receive an invite to the event: 
- SALs/SDRs should add members to campaign to be invited to event, using the appropriate `Campaign Status`:  
    - `Sales Invited` = You have personally invited the person to the event.
    - `Sales Nominated` = Marketing will invite the person on your behalf. They will receive invitation email pre-event plus any confirmations/reminders if they register.
    - `Marketing Invited` = Marketing will be sending geo-targted invitation emails to specific events pulling the names from our active database. Anyone included in this master send will be added to the campaign with this status.
    - **Any other Status** = Do not assign any other status to records. The campaign members will be updated by MPM or automated through registration.
 
#### Employee Booth Guidelines

- Perfect your Pitch
   - Most people have two ears and one mouth. Successful pitching is two parts listening to one part talking. Be engaged and interested in folks that visit. Be genuinely curious about their story. Understand them first before you start telling them about us. 
   - A great way to start is to offer a handshake and say,
       1. "Hi, my name is [your_name]”
           1. “Hi I’m [their_name]”
       2. “[their_name, are you familiar with GitLab?”
           1. “Yes”
               1. “Thanks for using us, are you using just the source code management or are you also using the built-in GitLab CI/CD?”
           2. “No” - “Great, in a nutshell we <140 character description> - for example, what company do you work for?”
               1. “Company X”
               2. And what’s your role at company x?
                   1. “Title X”
               3. Tailor your pitch to their specific experience. Ask about what tools they are using today, what they like or dislike about those tools.
   - Working at the booth is a great place to try out different ways of explaining technology and trying out different value propositions to see what resonates the most.
   - Know some stock answers
   - You can ask them which talk they’ve heard so far has been the most interesting.
- Close the deal
   - Figure out what the next step for this person is. Are they a decision maker at a large org?
   - If a conversation is running long, get his/her info and schedule a time to chat or follow up at a later time outside of the booth. The goal of the booth is to make initial contact and connections.
  - Give a personal follow up
- Stand at the front of booth facing the crowd.
   - Don’t make folks walk into the booth and seek you out. Stand out at the front. Make eye contact and smile at folks walking by. If they stop or pause you can ask them, “Are you familiar with GitLab?”
- Hangout outside the booth.
   - Too many GitLab team-members in the booth discourages other folks from coming by. The booth can be a great place to meet up, but don’t hang out there. Move the conversation to a near by lounge or social area.
   - When you are at the booth keep conversation with your coworkers to a minimum.
   - When you are at the booth focus on serving the attendees.
   - Do not do normal daily work in the booth - the booth is not a place for taking calls, or responding to emails. When you are at the booth you are on booth duty and that is it.
- Keep an abundant tidy stash of swag out
   - During slow times, restock swag and tidy up booth.
- Keep the booth clean.
   - The booth should be clean and organized at all times.
- Avoid eating meals in booth, please keep lids on beverages and out of sight.
- If we have the bandwidth or the traffic is slow do not be afraid to walk around to other booths and talk to people. Make friends we could partner with, create interesting content with, or just have friendly beers.
- If press comes to the event feel free to put them in contact with CMO.
- Engage the competition.
   - Be friendly and polite to competitors to come by the booth.
- Don't forget your business cards.

#### Scanning Best Practices
- Be an active but polite Badge Scanner  
   1. Don’t reach for the badge without first asking if you can scan someone’s badge and don't lead a conversation with can I scan you. Ask folks politely, “Would you appreciate a follow up email?” or “Mind if I scan your badge?” Many folks will say, “yes.” If they say, “Not really.” You can say, “Great, we don’t want to clutter up your inbox. You can always go online to about.gitlab.com if you’d like to check back in with us.”
   3. Trade scans for all swag if the attendee consents. If we have the space and staff, someone should be in charge of distributing and organizing swag, and scanning folks who come by for swag.
- Take good notes:
   1. Your initials (This way the email can be more personalized to say, “We saw you chatted with [name] in the booth.”) Add your initials + “follow up” if you personally can send them an email within a week of the conference.
   2. The tech stack (what tools they are using)
   3. Any specifics needed in the follow up (schedule a call, send docs for X, interested in Y, etc.)

##### Suggested Attire  

- Wear at least one piece of branded GitLab clothing. If you prefer to wear something dressier than the GitLab branded items available that is also acceptable. Feel free to wear our sticker on your clothing. 
- If the conference is business casual try some nice jeans (no holes) or dress pants.
- Clean, closed-toed shoes please.
- A smile.

##### Booth Set Up  
- Bring:
   - Generic business cards
   - Stickers + any other swag
   - Events laptop (for slideshow) + charger + dongles
   - Backup power banks
   - Mints & hand sanitizer
   - One pagers + cheat sheets

##### Day of Booth Staffing   
- Ideally booth shifts will be around 3 hours or less.
- Staff more people during peak traffic hours.
- Avoid shift changes during peak hours.
- Aim to staff the booth with individuals with a variety of expertise and backgrounds- ideally technical and non-technical people from various departments should be paired.
- Send out invites on the Events & Sponsorship calendar to booth staff with the following information:
   - Time and date of event, booth, and shift
   - Any instructions on using or locating lead scanner
   - Any relevant event set up or clean up

#### Post Event  

- Add event debrief to event issue in marketing project. The debrief should include the following if applicable:
   - Was the event valuable?
       - Would you go again? Should we go again?
       - Did we get good leads/contacts? What was the audience profile like?
       - Best questions asked and conversations. Trends in questions asked.
       - Was our sponsorship/involvement successful? Did we go in at the proper sponsorship level?
   - How was the booth set up?
       - How was the booth staffing?
       - Did the booth get enough traffic?
       - Booth location and size
   - How did our swag go over?
       - Did we have enough/too much?
   - Contests
       - Did the contest(s) effectively build our brand and connecting with our target audience?

#### Event List  
1. List received by FMM from event organizers  
2. FMM reviews and cleans up list following the guidelines for [list imports](/handbook/business-ops/#list-imports)
3. List is sent to Marketing OPS for upload to SFDC & associate to related Campaign (w/in 24hrs of receipt from event)      
4. Marketing OPS creates CONTACT view in SFDC; assigns records based on Territory ownership.      
5. Marketing OPS notifies MPM/FMM when the list has been uploaded so the Marketing follow up email can be sent  
6. Marketing OPS posts in the event Issue & on `#sales`/`#sdrs_and_bdrs` slack channel - a link to SFDC campaign and a ink to SDFC CONTACT view 


Common lead questions:
- Record ownership will be assigned using the [Global Ownership](/handbook/business-ops/#global-account-ownership) rules 
- All followup needs to be tracked in SFDC  
- List upload needs to be done **before** follow up is done so we can ensure proper order of operations & attribution is given correctly  
- Record Owner and/or SDR doing follow up need to be sure to update the [`Contact Status`](/handbook/business-ops/#lead--contact-statuses) on the record as follow up is done.
- Campaign type & meaning of [Campaign Member status](/handbook/business-ops/#campaign-type--progression-status) can be found in the Business OPS handbook   

## Swag
- Swag selection and creation is managed by Corporate Marketing. All information on swag can be found in the [Corporate Marketing handbook](/handbook/marketing/corporate-marketing/#swag).


## Specifics for Community Relations 
## Specifics for Corporate Events
## Specifics for Field Events 
- For events where a field marketing representative cannot be present, the FM DRI will assign an onsite lead. The DRI will be responsible for coordinating with this person and providing he/she any info they will need to help run the event in their absence. This person will be the venue point of contact as well as responsible for set up and tear down. 
- FOR EMEA: We must ensure we are gathering GDPR compliant leads - Lead devices scanning follow up needs to be in event T&C. If GDPR is not in the T&C, we are not allowed to follow up on the leads. Scanning a lead is not automatically GDPR compliant if visitors have not agreed to it.

## NORAM Event Deadlines Process for MPM Tasks
* FMM changes main event issue to `STATUS: WIP` and assigns FMC to issue
* FMC attaches `FMC NAM - Event Tracking` label to issue
* MPM creates epic and related issues and assigns to FMM and FMC
* FMC adds all deadlines for MPM issues to bottom of main event issue
* FMC to confirm that each email issue has a calendar event created in the `Email Marketing Calendar` with a link to the issue, and that `add to calendar` has been checked in the issue
* FMM to update Copy Document file with content and keep link in description of issue
* FMM checks off tasks and pings the MPM in the corresponding issue as they are completed
* 1 business day before task is due, FMC to ping FMM in event issue with reminder

## NORAM Field Marketing Swag
The NORAM Field Marketing team utilizes our swag vendor, Nadel. Nadel is available to produce, ship and store swag and event material. The FMM is responsible for accessing the [Nadel Portal](https://rhu270.veracore.com/v5fmsnet/MONEY/OeFrame.asp?PmSess1=2095144&Action=LOGIN&pos=GIT476&v=4&OfferID=&sxref=) via the Field Marketing team login available in the 1Password Marketing Vault to order all swag and trade show items for their events.

Nadel Portal Demo - [View Here](https://drive.google.com/open?id=1JGGjmWioXmwKI8t1zowqyfl22GKeaOk3)    
Nadel Admin Portal Demo - [View Here](https://drive.google.com/open?id=1YNNjr-A8OJVLuod27vWfTCbEO1y21d9T)
- **Existing Items:** Item quantities are listed in the portal. Please select from the current items in stock for your event. If you need a larger quantity of an item over what is available, please reach out to `@krogel` for reordering.
- **New Items and Designs:** Requests for new swag items not already available in the Nadel portal require management approval. Any new swag designs must be approved by the brand team for brand consistency. Nadel will email all final designs to the brand team for approval. You can suggest new designs in the swag slack channel or more formally in an issue in the [Swag Project](https://gitlab.com/gitlab-com/swag_suggestions).
- **Large Orders:** For orders over 500 pieces, please do not pull from existing stock in the Nadel portal. Contact `@krogel` for assistance on placing a new order.
- **Shipping:** Please make sure to specify all shipping requirements and deadlines when ordering your items. Nadel will provide return shipping labels with each order for easy return shipping to the warehouse after your event.
- **Lead Times:** Please be aware that ordering newly designed swag or placing reorders for existing items requires adequate lead time. Timeframes vary greatly based on the items selected and design approval. General Lead Times: 6 weeks to produce a new item and 2-3 weeks to reorder current designs.
- **Stickers:** For sticker orders, please utilize our [Sticker Mule](https://www.stickermule.com/) account. Delivery options and timelines are provided during the ordering process. Any new sticker designs must be approved by the brand team for brand consistency. You can suggest new designs in the swag slack channel or more formally in an issue in the [Swag Project](https://gitlab.com/gitlab-com/swag_suggestions).

## NORAM Field Marketing Venue Search Requests
For venue search requests, FMM to [open an issue](https://gitlab.com/gitlab-com/marketing/field-marketing/issues/new?issuable_template=NAM_Venue_Search). Follow instructions to provide event details and assign to the FMC. This template can be utilized by the FMM to request a detailed venue search from the FMC. This search will include full proposals and event space details for multiple potential venues for an event.

## How to add events to the [about.gitlab.com/events](/events/) page

In an effort to publicly share where people can find GitLab at events in person throughout the world, we have created [about.gitlab.com/events](/events).  This page is to be updated by the person responsible for the event. To update the page, you will need to contribute to the [event master.yml](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/events.yml).
If you need more information about our exact involvement in an specific event please visit the marketing project in gitlab.com and search the name of the event for any related issues. The "Meta" issue should include the most thorough and high level details about each event we are participating in. Place your event in the order in which it is happening. The list runs from soonest to furthest in the future.
Save event images and headers here: Save images for featured events [here](https://gitlab.com/gitlab-com/www-gitlab-com/tree/master/source/images/events)

### Details to be included (all of which are mandatory in order for your MR to pass the build):

- **Topic** - Name of the event you would like to add
- **Type** - Please choose one of the following: `Diversity`, `Conference`,
`MeetUp`, `Speaking Engagement`, `Webinar`, `Community Event` or `GitLab Connect`. Events cannot have more than one type. If more than one apply, choose the best. If you feel your event doesn’t fit in the below category, do not just manually add a type. Please reach out to events@gitlab.com  to suggest a new type of event.
- **Date** - Start date of event
- **Date ends** - Day event ends
- **Description** - Brief overview about event (can be taken from event homepage).
- **Location** - city, state,provinces, districts, counties (etc depending on country), country where event will take place
- **Region** - `NORAM`, `LATAM`, `EMEA`, `APAC`, or `Online`
- **Social tags** - hashtag for event shared by event host
- **Event URL** - homepage for event

#### Example

```
- topic: The Best DevOps Conference Ever
 type: Conference
 date: January 1, 2050
 date_ends: January 3, 2050 # Month DD, YYYY
 description: |
              The Best DevOps Conference Ever brings together the best minds in the DevOps land. The conference consists of 3 full days of DevOps magic, literally magic. Attendees will have the opportunity to partake in fire talks and moderated sessions. This is one you won’t want to miss.
 location: Neverland, NVR
 region: APAC
 social_tags: DEVOPS4LIFE
 event_url: https://2050.bestdevops.org
```

#### Template

```
- topic:
 type:
 date:
 date_ends:
 description:
 location:
 region:
 social_tags:
 event_url:
```
For featured events include:
```
featured:
   background: background/image/src/here.png
```

### How to add an event specific landing page linking from the [about.gitlab.com/events page](/events/)

For corporate tradeshows we will want to create an event specific page that links from the [about.gitlab.com/events](/events/) page. The purpose of this page is to let people know additional details about GitLab’s presence at the event, how to get in touch with us at the event, and conference sessions we are speaking in (if applicable).  

For smaller Field Marketing shows we use Marketo landing pages vs. the events yml. By doing this, the MPMs own the creation of these pages and they are the only ones who will have edit access to these pages.

When to specifically use a Marketo landing page vs. the events yml:
1. This is an event owned by Field Marketing
1. The event cost the company less than $10,000 (or your country's equivalent)
1. We will be driving traffic to the marketo landing page for less than 1.5 months.

Steps to take to create the new page:

1. Create new a new branch of the [www-gitlab-com project.](https://gitlab.com/gitlab-com/www-gitlab-com). - Branch name should be what event you’ve added.
1. From new Branch, navigate to `Data`, then to `events.yml`
1. Scroll down to the area where its date appropriate to add the event
1. Add event using instructions in [handbook](/handbook/marketing/corporate-marketing/#how-to-add-an-event-to-the-eventsyml)
1. To create the event specific page you need to add a subset of the following information:

  - **url:** - you make this up based on what you want the URL to be from about.gitlab.com
  - **header_background:** choose from an image already in the images folder or add your own image. If you do not know how to do this, please watch [this tutorial](https://drive.google.com/open?id=14wCjHZLbcUUDArGYBNeinsHAlET6ubMy). 
  - **header_image:** choose from an image already in the images folder or add your own. (optional- if you prefer not to include, remove field altogether)
  - **header_description:** what CTA would you like the person to do on the page
  - **booth:** booth number at event, if there is no booth number, then remove this line of code (optional)
  - **form:** code that tells the system to add the contact info form. Marketing Ops will provide you with this number. They need to create a specific form for each page associated with a campaign in sfc.
  - **title:** CTA for why someone would want to give their contact info. Also used in `contact:` to distinguish a header title.
  - **description:** additional info on why someone would want to give their contact info
  - **number:** Marketo form number - Marketing Operations will need to give this number to you. Under `form:`
  - **content:** all of the information in the example section is all optional based on your event. If its not needed, simply delete.
1. Please watch [this tutorial](https://drive.google.com/open?id=14wCjHZLbcUUDArGYBNeinsHAlET6ubMy) for additional help.

### Example

```
Giving the following data will give this event it's own dedicated page on about.gitlab.com, must provide a unique url.
 If it is text, it needs to be wrapped in "double quotes". This is so you can use characters like : and ' without breaking anything.

 url: aws-reinvent
 header_background: /images/blogimages/gitlab-at-vue-conf/cover_image.jpg
 header_image: /images/events/aws-reinvent.svg
 header_description: "Drop by our booth or schedule a time, we'd love to chat!"
 booth: 2608
 form:
     title: "Schedule time to chat"
     description: "Learn more about how GitLab can simplify toolchain complexity and speeds up cycle times."
     number: 1592
 content:
   - title: "How to can get started with GitLab and AWS"
     list_links:
       - text: "Simple Deployment to Amazon EKS"
         link: "/2018/06/06/eks-gitlab-integration/"
       - text: "GitLab + Amazon Web Services"
         link: "/solutions/aws/"
       - text: "Top Five Cloud Trends"
         link: "/2018/08/02/top-five-cloud-trends/"
       - text: "How Jaguar Land Rover embraced CI to speed up their software lifecycle"
         link: "/2018/07/23/chris-hill-devops-enterprise-summit-talk/"
   - title: "Let's Meet!"
     body: "Join us for a live demo on getting started with Auto DevOps on Nov 28th at 1pm to learn how Auto DevOps simplifies your deployment pipeline to accelerate delivery by 200%, improves responsiveness, and closes the feedback gap between you and your users."
 speakers:
   - name: "Josh Lambert"
     title: "Senior Product Manger, Monitor"
     image: /images/team/joshua.jpg
     date: "Tuesday, Nov 28"
     time: "1:00pm PT"
     location: "Booth 2608 at the expo floor in the Venitian"
     topic: "GitLab CI 101"
     description: "In this talk, we will review how simple it is to get started with GitLab's built in CI tool."
   - name: "Reb"
     title: "Solutions Architect"
     image: /images/team/reb.jpg
     date: "Tuesday, Nov 27"
     time: "2:00pm PT"
     location: "Booth 2608"
```

### Template

```
Giving the following data will give this event it's own dedicated page on about.gitlab.com, must provide a unique url. If it is text, it needs to be wrapped in "double quotes". This is so you can use characters like : and ' without breaking anything.

 url:
 header_background:
 header_image:
 header_description:
 booth:
 form:
     title:
     description:
     number:
 content:
   - title:
     list_links:
       - text:
         link:
       - text:
         link:
       - text:
         link:
   - title:
     body:
 speakers:
   - name:
     title:
     image:
     date:
     time:
     location:
     topic:
     description:
   - name:
     title:
     image:
     date:
     time:
     location:
```
### Speaking at events 
If you’re looking for information about speaking at an events head over to our [Technical Evangelism page](/handbook/marketing/technical-evangelism/).  

