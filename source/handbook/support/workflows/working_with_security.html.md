---
layout: markdown_page
title: Working with Security
category: Security
---

### On this page
{:.no_toc}

- TOC
{:toc}

----

## Overview

Occasionally, users will reach out to security [at] gitlab [dot] com, following the [Responsible Disclosure Policy](/security/disclosure/), with questions that may be better addressed by Support (e.g., help resizing a repository in response to a mass notification).

Other times, users will reach out to Support to report a security issue.

______________

## General Guidelines

Support issues identified as needing [transfer to security](#identifying-issues-for-transfer-to-security) should be treated with the
same caution as any other suspicious email:

- Don't access any links included, especially if the sender intended to obfuscate links by using something like `hxxp://` or `hxxps://` or 'evil(.).com`.
- If there is any doubt, ask in `#security`, ask your manager or transfer the ticket.
- It's *better* to send along a ticket that Security evaluates and sends back than to mishandle an important report. Be biased to passing things to Security.

### Identifying Issues for Transfer to Security

 * Reports of phishing or malicious content, regardless of hosting source.  Keywords:
   * malware/malicious
   * phish
   * campaign
 * Sender claims to be a security related employee at a non-associated company.
 * DMCA Takedown requests
 * Security questionnaires or other requests for documentation.

### Identifying Issues for Transfer to Support

 * User is having problems with 2-factor or other authentication on their account.
 * A vulnerability report that has been triaged by the security team as `working as intended`,
 * Requires actions taken by Support to resolve.
 
## Workflows

### Transfer from Security to Support

In the case that something ended up in the Security inbox and was forwarded on via email:

1. Open the ticket in ZenDesk.
1. Depending on the content of the forward, you can usually change the requestor to the user. Sometimes, it's preferable to create a new ticket. In either case, proceed as if it's a regular ticket from a user.
1. Often, these tickets will lack the name and email address of the user. You can usually find the original email by searching in the **#security-alert-manual** channel (everything emailed to security [at] is also shared there). Should that search turn up short, feel free to reach out to the individual who forwarded the ticket for this information.

### Transfer from Support to Security

In the case that a security issue was reported through a Zendesk support ticket:

1. Change the *Form* to **Security Issue**.

### Escalate ZenDesk ticket to Security

In the case that a security issue was reported through a support ticket:

1. Update the assignee in ZenDesk to Security

1. Link to the issue reporting the vulnerability

### If the customer has already created an issue

In the case that the customer has already filed an issue for the vulnerability:

1. Mark the issue is `confidential`

1. Add `security`, `customer`, and `bug` or `feature proposal` labels

1. Assign [Severity and Priority Labels](/handbook/engineering/security/#severity-and-priority-labels-on-security-issues)

### If the customer has not yet created an issue

See [Creating New Security Issues](/handbook/engineering/security/#creating-new-security-issues)
