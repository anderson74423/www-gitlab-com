---
layout: markdown_page
title: "VUL.5.01 - Code Security Check Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# VUL.5.01 - Code Security Check

## Control Statement

Quarterly, GitLab conducts source code checks for vulnerabilities according to the service risk rating assignment.

## Context

By manually and automatically reviewing our source code for security vulnerabilities and best-practices, we can preemptively identify and address risks to our customers, GitLab teammembers, and partners. Code security checks also help us evaluate the consistency of secure coding standards and improve our application security training.

## Scope

This control applies to all GitLab source code.

## Ownership

TBD

## Implementation Guidance

For detailed implementation guidance relevant to GitLab team-members, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/VUL.5.01_code_security_check.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/VUL.5.01_code_security_check.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/VUL.5.01_code_security_check.md).

## Framework Mapping

* ISO
  * A.14.2.1
  * A.14.2.5
* SOC2 CC
  * CC7.1
  * CC8.1
* PCI
  * 6.3.1
  * 6.4.4
