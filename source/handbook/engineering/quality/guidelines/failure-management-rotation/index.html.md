---
layout: markdown_page
title: "QA Failure Management Rotation"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Overview
This is a schedule to share the responsibility of debugging/analysing the failures in the daily runs in:
* [Nightly pipeline](https://gitlab.com/gitlab-org/quality/nightly/pipelines): Tests are run [daily at 4:00 am UTC](https://gitlab.com/gitlab-org/quality/nightly/pipeline_schedules/9530/edit) and results are reported on the [#qa-nightly](https://gitlab.slack.com/messages/CGLMP1G7M) slack channel.
* [Staging pipeline](https://gitlab.com/gitlab-org/quality/staging/pipelines): Tests are run [daily at 4:00 am UTC](https://gitlab.com/gitlab-org/quality/staging/pipeline_schedules/9514/edit) and results are reported on the [#qa-staging](https://gitlab.slack.com/messages/CBS3YKMGD) slack channel.

Additional tests are also run as part of the release process:
* [Deployer pipeline](https://ops.gitlab.net/gitlab-com/gl-infra/deployer/pipelines): This will also report into [#qa-staging](https://gitlab.slack.com/messages/CBS3YKMGD) and [#qa-production](https://gitlab.slack.com/messages/CCNNKFP8B). Reporting will be enabled as part of some [upcoming changes to deployment automation](https://gitlab.com/gitlab-org/release/framework/issues/201).

Please refer to the Quality team guidelines on [debugging QA pipeline test failures](/handbook/engineering/quality/guidelines/debugging-qa-test-failures) for specific instructions on how to do an appropriate level of investigation and determine next steps for the failing test.

### Responsibility
* During the scheduled dates, the support tasks related to the test runs become the Directly Responsible Individual's ([DRI](/handbook/people-operations/directly-responsible-individuals)'s) highest priority.
* At the end of the schedule, the DRI should write a simple handoff note specifying the highlights of that week and post it in the [#quality](https://gitlab.slack.com/messages/C3JJET4Q6) slack channel and in the ["Quality Engineering weekly" meeting document](https://docs.google.com/document/d/12IcXnAkFcEIfi9EDZFaBcpIa8PrKIw7EL5KgSOeSTqw/edit?usp=sharing).

### Schedule

**July 2019 | August 2019 | September 2019**

| **Start Date** | **DRI**                                                     | **Secondary**                                               |
|----------------|-------------------------------------------------------------|-------------------------------------------------------------|
| 2019-07-08     | [Dan Davison](/company/team/#sircapsalot)                   | [Aleksandr Soborov](/company/team/#asoborov)                |
| 2019-07-15     | [Aleksandr Soborov](/company/team/#asoborov)                | [Sanad Liaquat](/company/team/#sanadliaquat)                |
| 2019-07-22     | [Sanad Liaquat](/company/team/#sanadliaquat)                | [Tomislav Nikić](/company/team/#asktomislav)                |
| 2019-07-29     | [Tomislav Nikić](/company/team/#asktomislav)                | [Walmyr Lima e Silva Filho](/company/team/#walmyrlimaesilv) |
| 2019-08-05     | [Walmyr Lima e Silva Filho](/company/team/#walmyrlimaesilv) | [Grant Young](/company/team/#grantyoung)                    |
| 2019-08-12     | [Grant Young](/company/team/#grantyoung)                    | [Mark Lapierre](/company/team/#mdlap)                       |
| 2019-08-19     | [Mark Lapierre](/company/team/#mdlap)                       | [Jennie Louie](/company/team/#jennielouie)                  |
| 2019-08-26     | [Jennie Louie](/company/team/#jennielouie)                  | [Zeff Morgan](/company/team/#zeffer)                        |
| 2019-09-02     | [Zeff Morgan](/company/team/#zeffer)                        | [Désirée Chevalier](/company/team/#dchevalier2)             |
| 2019-09-09     | [Désirée Chevalier](/company/team/#dchevalier2)             | [Dan Davison](/company/team/#sircapsalot)                   |
| 2019-09-16     | [Dan Davison](/company/team/#sircapsalot)                   | [Aleksandr Soborov](/company/team/#asoborov)                |
| 2019-09-23     | [Aleksandr Soborov](/company/team/#asoborov)                | [Sanad Liaquat](/company/team/#sanadliaquat)                |
| 2019-09-30     | [Sanad Liaquat](/company/team/#sanadliaquat)                | [Tomislav Nikić](/company/team/#asktomislav)                |

<details>
  <summary>October 2019 | November 2019 | December 2019</summary>

| **Start Date** | **DRI**                                                     | **Secondary**                                               |
|----------------|-------------------------------------------------------------|-------------------------------------------------------------|
| 2019-10-07     | [Tomislav Nikić](/company/team/#asktomislav)                | [Walmyr Lima e Silva Filho](/company/team/#walmyrlimaesilv) |
| 2019-10-14     | [Walmyr Lima e Silva Filho](/company/team/#walmyrlimaesilv) | [Grant Young](/company/team/#grantyoung)                    |
| 2019-10-21     | [Grant Young](/company/team/#grantyoung)                    | [Mark Lapierre](/company/team/#mdlap)                       |
| 2019-10-28     | [Mark Lapierre](/company/team/#mdlap)                       | [Jennie Louie](/company/team/#jennielouie)                  |
| 2019-11-04     | [Jennie Louie](/company/team/#jennielouie)                  | [Zeff Morgan](/company/team/#zeffer)                        |
| 2019-11-11     | [Zeff Morgan](/company/team/#zeffer)                        | [Désirée Chevalier](/company/team/#dchevalier2)             |
| 2019-11-18     | [Désirée Chevalier](/company/team/#dchevalier2)             | [Dan Davison](/company/team/#sircapsalot)                   |
| 2019-11-25     | [Dan Davison](/company/team/#sircapsalot)                   | [Aleksandr Soborov](/company/team/#asoborov)                |
| 2019-12-02     | [Aleksandr Soborov](/company/team/#asoborov)                | [Sanad Liaquat](/company/team/#sanadliaquat)                |
| 2019-12-09     | [Sanad Liaquat](/company/team/#sanadliaquat)                | [Tomislav Nikić](/company/team/#asktomislav)                |
| 2019-12-16     | [Tomislav Nikić](/company/team/#asktomislav)                | [Walmyr Lima e Silva Filho](/company/team/#walmyrlimaesilv) |
| 2019-12-23     | [Walmyr Lima e Silva Filho](/company/team/#walmyrlimaesilv) | [Grant Young](/company/team/#grantyoung)                    |
| 2019-12-30     | [Grant Young](/company/team/#grantyoung)                    | [Mark Lapierre](/company/team/#mdlap)                       |
</details>

### Responsibilities of the DRI and Secondary for scheduled pipelines
* The DRI does the [triage](/handbook/engineering/quality/guidelines/debugging-qa-test-failures/#steps-for-debugging-qa-pipeline-test-failures) and they let the counterpart TAE know of the failure.
* The DRI makes the call whether to fix or quarantine the test.
* The fix/quarantine MR should be reviewed by either the Secondary or the counterpart TAE (based on whoever is available). If both of them are not available immediately, then any other TAE can review the MR.  In any case, both the Secondary and the counterpart TAE are always CC-ed in all communications.
* The DRI should periodically take a look at the list of unassigned quarantined issues in [staging](https://gitlab.com/gitlab-org/quality/staging/issues) and in [nightly](https://gitlab.com/gitlab-org/quality/nightly/issues) and work on them.
* If the [DRI](/handbook/people-operations/directly-responsible-individuals) is not available during their scheduled dates (for more than 2 days), they can swap their schedule with another team member. If the DRI's unavailability during schedule is less than 2 days, the Secondary can help with the tasks.

### Responsibilities of the DRI and Secondary for deployment pipelines
* The DRI helps the delivery team debug test failures that are affecting the release process.
* The Secondary fills in when the DRI is not available.
