---
layout: markdown_page
title: "Legal Roles"
---

For an overview of all legal roles please see the [roles directory in the repository](https://gitlab.com/gitlab-com/www-gitlab-com/tree/master/source/job-families/legal).

[Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#s-group)
