---
layout: markdown_page
title: "FY20-Q3 OKRs"
---

This fiscal quarter will run from August 1, 2019 to October 31, 2019.

## On this page
{:.no_toc}

- TOC
{:toc}

### 1. CEO: IACV.

1. VP Engineering: [Enterprise-level availability](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4893)
    1. Development: [Improve Predictability around customer expectations](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4841)
    1. Infrastructure: [Raise GitLab.com Availability from 96.64% to a consistent 99.95%](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4918).
    2. Security: [Mitigate and track H1 spend with secure coding training](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4924)
    1. Support: [Create sustainable processes to achieve 95% performance for First Response SLA](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4920)
    1. Support: [Create sustainable processes to achieve 85% performance to Next Response Time SLA](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4921)
    1. Quality: [Improve self-managed enterprise upgrade robustness](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4852)

### 2. CEO: Popular next generation product.

1. VP Product: Establish a new [Problem Validation](https://about.gitlab.com/handbook/product-development-flow/#validation-phase-2-problem-validation) process. Complete at least 15 problem validation cycles (one per PM, excluding Growth). Complete at least 5 qualitative customer interviews per PM.
1. VP Product: Improve the quality of product usage data, to enable better data driven product decisions. Finalize top Product and Growth [KPI definitions](/handbook/product/metrics/), including [SMAU](/handbook/product/metrics/#adoption) definitions for each stage.  Get reliable and accurate product usage data from both Self Hosted + SaaS customer environments. Deliver SMAU dashboards with actionable data for each stage.
1. VP Product: Ensure prioritization of work to deliver on UX key results to raise our [SUS score](/handbook/engineering/ux/performance-indicators/#perception-of-system-usability) to 76.2 and deliver ## foundational UI components in Q3. 
1. Director of Product, Dev; Director of Product, Ops; Director of Product, CI/CD: Communicate 1 year product Plans at the Section level to inform product investment decisions. Deliver 1 year plan content for Dev, Ops, and CI/CD sections by end of Q3. Stretch goal for Growth, Enablement, Secure, and Defend (pending Director hires).
1. VP of Product Strategy: Get strategic thinking into the org. 3 [strategy reviews](https://gitlab.com/gitlab-com/Product/issues/379) for key industry categories (project management, application security testing, CD/RA), 10 [stage strategy reviews](https://gitlab.com/gitlab-com/Product/issues/379).
1. VP of Product Strategy: Get acquisitions into shape; build a well-oiled machine. Complete valuation methodology for acquisitions (with FP&A).
1. VP Engineering: [Dogfood everything](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4894)
    1. Development: [Increase MRs, Maintain MR to Author ratio, first response KPI](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4837)
    1. UX: [Create products and experiences that users love and value](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4932)
    1. UX: [Increase product depth by improving existing product workflows based on customer validation](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4930)
    1. UX: [Provide best-in-class documentation for the top 10 Jobs to be Done (JTBD) by improving discovery and making content more relevant and usable](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4931)
    1. Security: [Improve Abuse ML detection and response](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4925)
    1. Support: [Increase support contributions to troubleshooting documentation to enable ticket deflection](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4922)
    1. Quality: [Increase engineering efficiency and remove test gaps in enterprise features](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4853)

### 3. CEO: Great team.

1. VP Product:  Hire at least 18 PM roles in Q3 to catch up with annual hiring plan.
1. VP Engineering: [Scale gracefully to 500+ members](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4895)
    1. Development: [Hire to Plan, New Manager Documenation, Increase Maintainers](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4838)
    1. Quality: [Grow the department to plan and improve onboarding experience](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4854)
    1. Security: [Build next phase of zero-trust roadmap](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4926)
